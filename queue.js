let collection = [];

// Write the queue functions below.

const print = () => {
    return collection;
};

const enqueue = (person) => {
    collection[collection.length] = person;
    return collection;
};

const dequeue = (person) => {
    const [removed, ...newArr] = collection;
    collection = newArr;
    return collection;
};

const front = () => {
    return collection[0];
};

const size = () => {
    return collection.length;
};

const isEmpty = () => {
    if (collection.length == 0 || collection === undefined) {
        return true;
    } else {
        return false;
    }
};

module.exports = { print, enqueue, dequeue, front, size, isEmpty };
